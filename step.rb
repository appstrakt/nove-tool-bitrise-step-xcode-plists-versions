require 'xcodeproj'
require 'fileutils'

def resolve_user_defined_settings(path, build_settings)
  # Replace all occurrences of $(VARIABLE) with the corresponding value in build settings
  path.gsub(/\$\(([^)]+)\)/) do |match|
    variable_name = $1
    build_settings[variable_name] || match # Replace if found, or leave untouched if not found
  end
end

def find_project_level_setting(project, setting, configuration_name)
  project.root_object.build_configuration_list.build_configurations
    .select { |config| config.name == configuration_name } # Filter by configuration_name
    .map { |config| config.build_settings[setting] }
    .compact
    .first
end

begin
  project_path = ENV['project_path']
  bundle_version = ENV['bundle_version']
  bundle_version_short = ENV['bundle_version_short']

  unless project_path && project_path.length > 0
    puts "No valid project_path was defined"
    exit 1
  end

  unless File.directory?(project_path)
    puts project_path + " doesn't exists!"
    exit 1
  end

  ext = File.extname(project_path)

  unless ext == '.xcodeproj'
    puts project_path + ' is not a valid Xcode Project!'
    exit 1
  end

  project = Xcodeproj::Project.open(project_path)

  unless project
    puts project_path + ' is not a valid .xcodeproj!'
    exit 1
  end

  target_found = false
  configuration_found = false
  enable_bitcode = false

  plist_paths = []
  project.targets.each do |target_obj|
    target_obj.build_configuration_list.build_configurations.each do |build_configuration|
      build_settings = build_configuration.build_settings
      plist_path = build_settings['INFOPLIST_FILE'].dup

      if !plist_path
        plist_path = find_project_level_setting(project, 'INFOPLIST_FILE', build_configuration.name)
      end

      if plist_path
        plist_path = resolve_user_defined_settings(plist_path, build_settings)
        plist_path.gsub! '$(SRCROOT)', ''
        plist_path = "#{File.dirname(project_path)}/#{plist_path}"
        plist_paths.push(plist_path)
      end
    end
  end

  # Remove all paths that don't exist
  plist_paths.uniq!
  plist_paths.reject! { |path| !File.exist?(path) }



  project.save()

  if plist_paths.length > 0
    if bundle_version && bundle_version.length > 0
      puts "Writing version '#{bundle_version}'"
    end

    if bundle_version_short && bundle_version_short.length > 0
      puts "Writing short version '#{bundle_version_short}'"
    end

    puts "----------EDIT FILES------------"

    plist_paths.each do | plist_path |
      plist_file = Xcodeproj::Plist.read_from_path(plist_path)

      puts plist_path

      if bundle_version && bundle_version.length > 0
        plist_file['CFBundleVersion'] = bundle_version
      end

      if bundle_version_short && bundle_version_short.length > 0
        plist_file['CFBundleShortVersionString'] = bundle_version_short
      end

      Xcodeproj::Plist.write_to_path(plist_file, plist_path)
    end

    puts "-----------END EDIT-------------"
  end

rescue => ex
  puts
  puts 'Error:'
  puts ex.to_s
  puts
  puts 'Stacktrace (for debugging):'
  puts ex.backtrace.join("\n").to_s
  exit 1
end
